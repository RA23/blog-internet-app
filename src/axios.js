import Axios from 'axios';

const axios = Axios.create({
	// baseURL: "http://api.my-app.test",
	// baseURL: 'http://localhost:8000',
	// baseURL: 'https://blog.developocc.com',
	baseURL: 'https://blogbackend.raymundoaparicio.com.mx',
	withCredentials: true,
	headers: {
		"Content-Type": "application/json",
		"Accept": "application/json",
		"X-Requested-With": 'XMLHttpRequest',
	},
	withXSRFToken: true
});

axios.interceptors.request.use(
	function(config) {
		const token = localStorage.getItem("token"); 
		if (token) {
			config.headers["Authorization"] = 'Bearer ' + token;
		}
		return config;
	},
	function(error) {
		return Promise.reject(error);
	}
)

export default axios;