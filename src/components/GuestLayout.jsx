import React, { useState, useEffect } from 'react';
import { NavLink, Outlet, useNavigate } from 'react-router-dom';
import { Container, Toolbar, Button, Typography, Box, Link } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import { useAuth } from '../contexts/AuthContext';
import axios from '../axios';
import { Search, SearchIconWrapper, StyledInputBase } from './Search';
import Swal from 'sweetalert2';

export default function GuestLayout() {
	const { setUser, user } = useAuth();
	const navigate = useNavigate();
	const [searchText, setSearchText] = useState('');
	const [isOnline, setIsOnline] = useState(navigator.onLine);

	useEffect(() => {
		// Actualizacion del estatus de Conexión
		const handleStatusChange = () => {
		  setIsOnline(navigator.onLine);

		};
	
		// Escuchar el estatus en línea
		window.addEventListener('online', handleStatusChange);
	
		// Escuchar el estatus fuera de línea
		window.addEventListener('offline', handleStatusChange);
	
		// Limpiar después de cada efecto para mejorar el rendimiento.
		return () => {
		  window.removeEventListener('online', handleStatusChange);
		  window.removeEventListener('offline', handleStatusChange);
		};
	}, [isOnline]);

	if(!isOnline) {
		const Toast = Swal.mixin({
			toast: true,
			position: "top-end",
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			didOpen: (toast) => {
			  toast.onmouseenter = Swal.stopTimer;
			  toast.onmouseleave = Swal.resumeTimer;
			}
		});
		Toast.fire({
			icon: "error",
			title: "No dispones de conexión a Internet"
		});
	}

	const handleLogout = async () => {
		try {
			const resp = await axios.post('/api/logout');
			if (resp.status === 200) {
				localStorage.removeItem('user');
				localStorage.removeItem('token');
				setUser();
				navigate('/');
			}
		} catch (error) {
			console.log(error);
		}
	};

	const keyboardEvents = (event) =>{
		if(event.key === 'Enter'){
			navigate(`/result/${searchText}`);
		}
	}

	const handleChange = event => {
		setSearchText(event.target.value);
	};

	return (
		<>
			<Container maxWidth="lg">
				<Toolbar sx={{ borderBottom: 1, borderColor: 'divider', mb: 6 }}>
					{ isOnline && (user == undefined ? (
						<NavLink to={'/login'}>
							<Button size="small">Iniciar Sesión</Button>
						</NavLink>
					) : (
						<NavLink to={'/profile'}>
							<Button size="small">{user.name}</Button>
						</NavLink>
					))}
					<NavLink to={'/'} style={{margin: '0 auto', display: "flex", textDecoration: 'none'}}>
						<Button size="small">Blog</Button>
					</NavLink>
					{ isOnline && (
						<Search>
							<SearchIconWrapper>
								<SearchIcon />
							</SearchIconWrapper>
							<StyledInputBase
								placeholder="Búsqueda..."
								inputProps={{ 'aria-label': 'search' }}
								onKeyDown={ keyboardEvents }
								value={searchText}
								onChange={handleChange}
							/>
						</Search>
					)}
					{ isOnline && (user == undefined ? (
						<NavLink to={'/register'}>
							<Button variant="outlined" size="small">
								Registrarse
							</Button>
						</NavLink>
					) : (
						<Button size="small" onClick={() => handleLogout()}>Cerrar Sesión</Button>
					))}
				</Toolbar>
				<Outlet />
			</Container>
			<Box component="footer" sx={{ bgcolor: 'background.paper', py: 6 }}>
				<Container maxWidth="lg">
					<Typography variant="h6" align="center" gutterBottom>
						Blog
					</Typography>
					<Typography variant="body2" color="text.secondary" align="center">
						{'Copyright © '}
						<Link color="inherit" href="/">
							Internet
						</Link>{' '}
						{new Date().getFullYear()}
						{'.'}
					</Typography>
				</Container>
			</Box>
		</>
	);
}