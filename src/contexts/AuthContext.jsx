import { createContext, useContext, useState } from 'react';
import axios from '../axios';

const AuthContent = createContext({
	user: null,
	setUser: () => {},
	csrfToken: () => {},
	getEntradas: () => {},
});

export const AuthProvider = ({ children }) => {
	const [user, _setUser] = useState(
		JSON.parse(localStorage.getItem('user')) || null
	);

	// Configurar el usuario en el almacenamiento local
	const setUser = (user) => {
		if (user) {
			localStorage.setItem('user', JSON.stringify(user));
		} else {
			localStorage.removeItem('user');
		}
		_setUser(user);
	};

	// Generación de tokens csrf para métodos invitados
	const csrfToken = async () => {
		await axios.get('/sanctum/csrf-cookie');
		return true;
	};

	// Guardar Entradas en el almacenamiento local
	const getEntradas = async () => {
		try {
			if(navigator.onLine){
				const resp = await axios.get('/api/tickets');
				if (resp.status === 200) {
					localStorage.setItem("entradas", JSON.stringify(resp.data));
				}
			}
		} catch (error) {
			localStorage.removeItem('entradas');
			throw new Error(error)
		}
	};

	return (
		<AuthContent.Provider value={{ user, setUser, csrfToken, getEntradas }}>
			{children}
		</AuthContent.Provider>
	);
};

export const useAuth = () => {
	return useContext(AuthContent);
};