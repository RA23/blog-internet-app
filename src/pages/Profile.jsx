import React, {useState, useEffect} from 'react';
import { useAuth } from '../contexts/AuthContext';
import axios from '../axios';
import { Paper, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Button, Grid, Modal, Box, Typography, TextField, Alert } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import Swal from 'sweetalert2';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { DateField } from '@mui/x-date-pickers/DateField';
import { useNavigate } from 'react-router-dom';

export default function Profile() {
	const { user } = useAuth();

	const [entradas, setEntradas] = useState([]);
	const [open, setOpen] = useState(false);
  	const handleOpen = () => setOpen(true);
  	const handleClose = () => setOpen(false);
	const [error, setError] = useState(null);

	// State para Nueva Entrada
	const [tituloError, setTituloError] = useState('');
	const [autorError, setAutorError] = useState('');
	const [fechaError, setFechaError] = useState('');
	const [contenidoError, setContenidoError] = useState('');

	const navigate = useNavigate();

	useEffect(() => {
		fetchEntradas()
	}, []);

	const fetchEntradas = async () => {
		try {
			const resp = await axios.get(`/api/tickets/own/${user.id}`);
			if (resp.status === 200) {
				setEntradas(resp.data);
			}
		} catch (error) {
			if (error.response && error.response.status === 500) {
				setEntradas([]);
			}else{
				navigate('/');
			}
		}
    }

	// Agregar una nueva Entrada
	const añadirEntrada = async (e) => {
		e.preventDefault();
		const { titulo, autor, fecha_publicacion, contenido } = e.target.elements;
		const body = {
			titulo: titulo.value,
			autor: autor.value,
			fecha_publicacion: fecha_publicacion.value,
			contenido: contenido.value,
			user_id: user.id,
		};
		try {
			const resp = await axios.post('/api/tickets', body);
			if (resp.status === 200) {
				Swal.fire({
					text: resp.data.message,
					icon:"success",
				})
				handleClose();
				fetchEntradas();
				setTituloError('');
				setAutorError('');
				setFechaError('');
				setContenidoError('');
			}
		} catch (error) {
			if (error.response.status === 422) {
				if (error.response.data.error.titulo) {
					setTituloError(error.response.data.error.titulo[0]);
				} else {
					setTituloError('');
				}
				if (error.response.data.error.autor) {
					setAutorError(error.response.data.error.autor[0]);
				} else {
					setAutorError('');
				}
				if (error.response.data.error.fecha_publicacion) {
					setFechaError(error.response.data.error.fecha_publicacion[0]);
				} else {
					setFechaError('');
				}
				if (error.response.data.error.contenido) {
					setContenidoError(error.response.data.error.contenido[0]);
				} else {
					setContenidoError('');
				}
			}
			if (error.response.status === 500) {
				setError('Ocurrió un error');
			} else {
				setError('');
			}
		}
    }

	// Eliminar una entrada
	const eliminarEntrada = async (id) => {
        const isConfirm = await Swal.fire({
            title: 'Estás seguro?',
            text: "No podrás revertir esto.!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, borralo!'
          }).then((result) => {
            return result.isConfirmed
          });

          if(!isConfirm){
            return;
          }

          await axios.delete(`/api/tickets/${id}`).then(({data})=>{
            Swal.fire({
                icon:"success",
                text:data.message
            })
            fetchEntradas()
          }).catch(()=>{
            Swal.fire({
                text: 'Ocurrió un error al eliminar',
                icon:"error"
            })
          })
    }

	const style = {
		position: 'absolute',
		top: '50%',
		left: '50%',
		transform: 'translate(-50%, -50%)',
		width: 400,
		bgcolor: 'background.paper',
		borderRadius: 2,
		boxShadow: 24,
		p: 4,
	};

	return (
		<>
			<Grid container spacing={2} justify="flex-end" alignItems="center">
            	<Grid item xs={12} sm={6}>
					<h2>Mis Entradas</h2>
				</Grid>
				<Grid item xs={12} sm={6} align="right">
					<Button variant="contained" color="success" onClick={handleOpen}>
						<AddCircleIcon />
					</Button>
				</Grid>
			</Grid>
			<TableContainer component={Paper}>
				<Table sx={{ minWidth: 650 }} aria-label="simple table">
					<TableHead>
					<TableRow>
						<TableCell align="center">Título</TableCell>
						<TableCell align="center">Fecha</TableCell>
						<TableCell align="center">Autor</TableCell>
						<TableCell align="center">Contenido</TableCell>
						<TableCell align="center"></TableCell>
					</TableRow>
					</TableHead>
					<TableBody>
					{
						entradas.length > 0 && (
							entradas.map((row, key) => (
								<TableRow
								key={key}
								sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
								>
								<TableCell align="center" component="th" scope="row">
									{row.titulo}
								</TableCell>
								<TableCell align="center">{row.fecha_publicacion}</TableCell>
								<TableCell align="center">{row.autor}</TableCell>
								<TableCell align="center">{row.contenido}</TableCell>
								<TableCell align="center">
								<Button variant="contained" color="error" onClick={()=>eliminarEntrada(row.id)}>
									<DeleteIcon />
								</Button>
								</TableCell>
								</TableRow>
							))
						)
					}
					</TableBody>
				</Table>
			</TableContainer>
			<Modal
				open={open}
				onClose={handleClose}
				aria-labelledby="modal-modal-title"
				aria-describedby="modal-modal-description"
			>
				<Box sx={style}>
					<Typography id="modal-modal-title" variant="h6" component="h2">
						Nueva Entrada
					</Typography>
					<Box component="form" onSubmit={añadirEntrada} noValidate sx={{ mt: 1 }}>
						{error && (
							<Alert severity="error">{error}</Alert>
						)}
						<TextField
						margin="normal"
						required
						fullWidth
						id="titulo"
						label="Título"
						name="titulo"
						autoFocus
						error={ tituloError ? true : false }
						helperText={ tituloError && (tituloError) }
						/>
						<TextField
						margin="normal"
						required
						fullWidth
						name="autor"
						label="Autor"
						id="autor"
						error={ autorError ? true : false }
						helperText={ autorError && (autorError) }
						/>
						<LocalizationProvider dateAdapter={AdapterDayjs}>
							<DateField 
							label="Fecha de Publicación" 
							margin="normal"
							required
							fullWidth
							name="fecha_publicacion"
							id="fecha_publicacion"
							format="DD-MM-YYYY"
							error={ fechaError ? true : false }
							helperText={ fechaError && (fechaError) }
							/>
						</LocalizationProvider>
						<TextField
						margin="normal"
						required
						fullWidth
						name="contenido"
						label="Contenido"
						id="contenido"
						error={ contenidoError ? true : false }
						helperText={ contenidoError && (contenidoError) }
						/>
						<Button
						type="submit"
						fullWidth
						variant="contained"
						sx={{ mt: 3, mb: 2 }}
						>
						Guardar
						</Button>
					</Box>
				</Box>
			</Modal>
		</>
	);
}