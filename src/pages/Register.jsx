import React, { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import axios from '../axios';
import { useAuth } from '../contexts/AuthContext';
import { Button, Typography, Box, Avatar, TextField, Grid, Alert } from '@mui/material';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';

export default function Register() {
	const { setUser, user } = useAuth();
	const [nameError, setNameError] = useState('');
	const [emailError, setEmailError] = useState('');
	const [passwordError, setPasswordError] = useState('');
	const [error, setError] = useState(null);
	const navigate = useNavigate();

	useEffect(() => {
		// Si el usuario esta Logueado regresar a la página Principal
		if (user) {
            navigate('/');
        }
	}, []);

	// Registrar Usuario
	const handleSubmit = async (e) => {
		e.preventDefault();
		const { name, email, password, cpassword } = e.target.elements;
		const body = {
			name: name.value,
			email: email.value,
			password: password.value,
			password_confirmation: cpassword.value,
		};
		try {
			const resp = await axios.post('/api/register', body);
			if (resp.status === 200) {
				localStorage.setItem('token', resp.data.token);
				setUser(resp.data.user);
				navigate('/');
			}
		} catch (error) {
			if (error.response.status === 422) {
				if (error.response.data.error.name) {
					setNameError(error.response.data.error.name[0]);
				} else {
					setNameError('');
				}
				if (error.response.data.error.email) {
					setEmailError(error.response.data.error.email[0]);
				} else {
					setEmailError('');
				}
				if (error.response.data.error.password) {
					setPasswordError(error.response.data.error.password[0]);
				} else {
					setPasswordError('');
				}
			}
			if (error.response.status === 401) {
				setError(error.response.data.message);
			} else {
				setError('');
			}
		}
	};

	return (
		<>
		<Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
			<Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
				<LockOutlinedIcon />
			</Avatar>
			<Typography component="h1" variant="h5">
				Registrarse
			</Typography>
			<Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3 }}>
				{error && (
					<Alert severity="error">{error}</Alert>
				)}
				<Grid container spacing={2}>
				<Grid item xs={12}>
					<TextField
					autoComplete="given-name"
					name="name"
					required
					fullWidth
					id="name"
					label="Nombre"
					autoFocus
					error={ nameError ? true : false }
					helperText={ nameError && (nameError) }
					/>
				</Grid>
				<Grid item xs={12}>
					<TextField
					required
					fullWidth
					id="email"
					label="Correo Electrónico"
					name="email"
					autoComplete="email"
					error={ emailError ? true : false }
					helperText={ emailError && (emailError) }
					/>
				</Grid>
				<Grid item xs={12}>
					<TextField
					required
					fullWidth
					name="password"
					label="Contraseña"
					type="password"
					id="password"
					autoComplete="new-password"
					error={ passwordError ? true : false }
					helperText={ passwordError && (passwordError) }
					/>
				</Grid>
				<Grid item xs={12}>
					<TextField
					required
					fullWidth
					name="cpassword"
					label="Confirmar Contraseña"
					type="password"
					id="cpassword"
					/>
				</Grid>
				</Grid>
				<Button
				type="submit"
				fullWidth
				variant="contained"
				sx={{ mt: 3, mb: 2 }}
				>
				Registrarme
				</Button>
				<Grid container justifyContent="flex-end">
				<Grid item>
					<Link to="/login" variant="body2">
						{"¿Ya tienes una cuenta? Inicia sesión"}
					</Link>
				</Grid>
				</Grid>
			</Box>
        </Box>
		</>
	);
}