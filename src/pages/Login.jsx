import React, { useState, useEffect  } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from '../axios';
import { Link } from 'react-router-dom';
import { useAuth } from '../contexts/AuthContext';
import { Button, Typography, Box, Avatar, TextField, FormControlLabel, Grid, Checkbox, Alert } from '@mui/material';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';

export default function Login() {
	const { setUser, csrfToken, user } = useAuth();
	const [error, setError] = useState(null);
	const [emailError, setEmailError] = useState('');
	const [passwordError, setPasswordError] = useState('');
	const navigate = useNavigate();

	useEffect(() => {
		// Si el usuario esta Logueado regresar a la página Principal
		if (user) {
            navigate('/');
        }
	}, []);

	// Inicio de Sesión
	const handleSubmit = async (e) => {
		e.preventDefault();
		const { email, password } = e.target.elements;
		const body = {
			email: email.value,
			password: password.value,
		};
		await csrfToken();
		try {
			const resp = await axios.post('/api/login', body);
			if (resp.status === 200) {
				localStorage.setItem('token', resp.data.token);
				setUser(resp.data.user);
				navigate('/');
			}
		} catch (error) {
			if (error.response.status === 422) {
				if (error.response.data.error.email) {
					setEmailError(error.response.data.error.email[0]);
				} else {
					setEmailError('');
				}
				if (error.response.data.error.password) {
					setPasswordError(error.response.data.error.password[0]);
				} else {
					setPasswordError('');
				}
			}
			if (error.response.status === 401) {
				setError(error.response.data.message);
			} else {
				setError('');
			}
		}
	};

	return (
		<>
			<Box
			sx={{
				marginTop: 8,
				display: 'flex',
				flexDirection: 'column',
				alignItems: 'center',
			}}
			>
			<Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
				<LockOutlinedIcon />
			</Avatar>
			<Typography component="h1" variant="h5">
				Iniciar Sesión
			</Typography>
			<Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
				{error && (
					<Alert severity="error">{error}</Alert>
				)}
				<TextField
				margin="normal"
				required
				fullWidth
				id="email"
				label="Correo Electrónico"
				name="email"
				autoComplete="email"
				autoFocus
				error={ emailError ? true : false }
				helperText={ emailError && (emailError) }
				/>
				<TextField
				margin="normal"
				required
				fullWidth
				name="password"
				label="Contraseña"
				type="password"
				id="password"
				autoComplete="current-password"
				error={ passwordError ? true : false }
				helperText={ passwordError && (passwordError) }
				/>
				<FormControlLabel
				control={<Checkbox value="remember" color="primary" />}
				label="Recuérdame"
				/>
				<Button
				type="submit"
				fullWidth
				variant="contained"
				sx={{ mt: 3, mb: 2 }}
				>
				Iniciar
				</Button>
				<Grid container>
				<Grid item>
					<Link to="/register" variant="body2">
						{"¿No tienes una cuenta? Registrarse"}
					</Link>
				</Grid>
				</Grid>
			</Box>
			</Box>
		</>
	);
}