import React, { useEffect, useState  } from 'react';
import { Grid, CardActionArea, Card, CardContent, Typography, Modal, Box } from '@mui/material';
import { useAuth } from '../contexts/AuthContext';

export default function Entradas() {
	const { getEntradas } = useAuth();
	const [entradas, setEntradas] = useState([]);
	const [open, setOpen] = useState(false);
	const [entrada, setEntrada] = useState({});

	useEffect(() => {
		fetchEntradas()
	}, []);

	const fetchEntradas = async () => {
		try {
			await getEntradas();
			const entradas = JSON.parse(localStorage.getItem("entradas"));
			setEntradas(entradas);
		} catch (error) {
			console.log(error);
			setEntradas([]);
		}	
    }

	// Cortar el texto a setenta caracteres 
	const cortarCaracteres = (texto) => {
		const cut = texto.slice(0, 70); 
		return cut;
	}

	// Abrir modal y agregar informacion de la entrada
	const openModal = async (modal, id) => {
		setOpen(modal);
		if(modal){
			try {
				const obj = entradas.find(o => o.id === id);
                setEntrada(obj);
			} catch (error) {
				setEntrada({});
			}
		}
	}

	const style = {
		position: 'absolute',
		top: '50%',
		left: '50%',
		transform: 'translate(-50%, -50%)',
		width: 400,
		bgcolor: 'background.paper',
		borderRadius: 2,
		boxShadow: 24,
		p: 4,
	};

	return (
		<>
			<Grid container spacing={4}>
			{
				entradas.length > 0 && (
					entradas.map((row, key)=>(
						<Grid item xs={12} md={6} key={key}>
							<CardActionArea component="a" onClick={() => openModal(true, row.id)}>
								<Card sx={{ display: 'flex' }}>
								<CardContent sx={{ flex: 1 }}>
									<Typography component="h2" variant="h5">
										{row.titulo}
									</Typography>
									<Typography variant="subtitle1" color="text.secondary">
										{row.fecha_publicacion}
									</Typography>
									<Typography variant="subtitle1" paragraph>
										{ cortarCaracteres(row.contenido) }...
									</Typography>
									<Typography variant="subtitle1" color="text.secondary">
										<i>Autor: {row.autor}</i>
									</Typography>
									<Typography variant="subtitle1" color="primary">
										Sigue leyendo...
									</Typography>
								</CardContent>
								</Card>
							</CardActionArea>
						</Grid>
					))
				)
			}
          	</Grid>
			<Modal
				open={open}
				onClose={() => openModal(false)}
				aria-labelledby="modal-modal-title"
				aria-describedby="modal-modal-description"
			>
				<Box sx={style}>
					<Typography id="modal-modal-title" variant="h6" component="h2">
						{entrada.titulo}
					</Typography>
					<Typography variant="subtitle1" color="text.secondary">
						{entrada.fecha_publicacion}
					</Typography>
					<Typography id="modal-modal-description" sx={{ my: 2 }}>
						{entrada.contenido}
					</Typography>
					<Typography variant="subtitle1" color="text.secondary">
						<i>Autor: {entrada.autor}</i>
					</Typography>
				</Box>
			</Modal>
		</>
	);
}