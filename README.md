## React + Vite

## Requesitos de Ejecución

- nodeJs >= 20.x

## Comandos de Ejecución

- Clonar el repositorio con git clone.
- Ejecutar npm install
- Ejecutar npm run dev
- Ir la URL principal (http://localhost:3000)

## Uso
- Para añadir una nueva Entrada, primero debe Registrarse
- Despues debe iniciar sesión
- Dar click sobre su nombre en la esquina superior izquierda
- Dar click en el botón verde y completar los campos

## Página en línea
- Ver (https://web.developocc.com/)